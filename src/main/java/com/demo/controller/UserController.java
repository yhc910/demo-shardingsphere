package com.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.demo.entity.User;
import com.demo.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    UserMapper userMapper;

    @RequestMapping("create")
    public int create(@RequestParam("name") String name) {
        log.info("# user create, name:{}", name);

        User user = new User();
        user.setName(name);
        user.setCreateDate(LocalDateTime.now());

        return userMapper.insert(user);
    }

    @RequestMapping("get")
    public User create(@RequestParam("id") Long id) {
        log.info("# user create, id:{}", id);
        return userMapper.selectById(id);
    }



}
