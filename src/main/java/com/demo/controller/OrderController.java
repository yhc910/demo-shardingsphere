package com.demo.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.demo.entity.Order;
import com.demo.entity.User;
import com.demo.mapper.OrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("order")
public class OrderController {

    private static final DateTimeFormatter datetimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Autowired
    OrderMapper orderMapper;

    @RequestMapping("add")
    public int add(@RequestParam("date") String date) {
        log.info("# order add, date:{}", date);

        Order order = new Order();
        order.setCreateDate(LocalDateTime.parse(date, datetimeFormatter));

        return orderMapper.insert(order);
    }

    @RequestMapping("list")
    public List<Order> list(@RequestParam("beginDate") String beginDate, @RequestParam("endDate") String endDate, @RequestParam("id") Long id) {
        log.info("# order create, beginDate:{} -> endDate:{}", beginDate, endDate);

        LambdaQueryWrapper<Order> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.between(Order::getCreateDate, LocalDateTime.parse(beginDate, datetimeFormatter), LocalDateTime.parse(endDate, datetimeFormatter));
//        queryWrapper.eq(Order::getCreateDate,LocalDateTime.parse(beginDate, datetimeFormatter));
//        queryWrapper.eq(Order::getId, id);
        return orderMapper.selectList(queryWrapper);
    }


}
