package com.demo.config.sharding.algorithm;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StaticValue {

    public static String userBaseTableMinDate;
    public static String userBaseTableMaxDate;

    @Value("${sharding.table.user.base.date.min}")
    public void setUserBaseTableMinDate(String userBaseTableMinDate) {
        this.userBaseTableMinDate = userBaseTableMinDate;
    }

    @Value("${sharding.table.user.base.date.max}")
    public void setUserBaseTableMaxDate(String userBaseTableMaxDate) {
        this.userBaseTableMaxDate = userBaseTableMaxDate;
    }

}
